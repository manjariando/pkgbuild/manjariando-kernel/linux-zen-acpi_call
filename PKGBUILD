# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Contributor: Maxime Gauduin <alucryd@gmail.com>
# Contributor: mortzu <me@mortzu.de>
# Contributor: fnord0 <fnord0@riseup.net>

_linuxprefix=linux-zen
_extramodules=extramodules-6.2-zen
pkgname=$_linuxprefix-acpi_call
_pkgname=acpi_call
_pkgver=1.2.2
pkgver=1.2.2_6.2.10.zen1_1
pkgrel=1
pkgdesc='A linux kernel module that enables calls to ACPI methods through /proc/acpi/call'
arch=('x86_64')
url="https://github.com/nix-community/acpi_call"
license=('GPL')
makedepends=("$_linuxprefix" "$_linuxprefix-headers")
provides=("$_pkgname")
groups=("$_linuxprefix-extramodules")
install=$_pkgname.install
source=("$_pkgname-$_pkgver.tar.gz::${url}/archive/refs/tags/v${_pkgver}.tar.gz")
sha256sums=('8b1902a94395c2fa5a97f81c94868a9cbc46a48e12309ad01626439bde96f1d9')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
  cd "${_pkgname}-${_pkgver}"
  make KVER="${_kernver}"
}

package() {
  _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
  depends=("${_linuxprefix}=${_ver}")

  cd "${_pkgname}-${_pkgver}"

  install -dm 755 "${pkgdir}"/usr/lib/{modules/${_extramodules},modules-load.d}
  install -m 644 acpi_call.ko "${pkgdir}"/usr/lib/modules/${_extramodules}/
  gzip "${pkgdir}"/usr/lib/modules/${_extramodules}/acpi_call.ko
  echo acpi_call > "${pkgdir}"/usr/lib/modules-load.d/${pkgname}.conf

  install -dm 755 "${pkgdir}"/usr/share/${pkgname}
  cp -dr --no-preserve='ownership' {examples,support} "${pkgdir}"/usr/share/${pkgname}/

  sed -i "s/EXTRAMODULES=.*/EXTRAMODULES=$_extramodules/" \
    "$startdir/acpi_call.install"
}
